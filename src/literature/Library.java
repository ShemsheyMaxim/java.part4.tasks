package literature;

import java.util.Calendar;
import java.util.Scanner;

/**
 * Created by Максим on 21.03.2017.
 */
public class Library {
    public static void main(String[] args) {
        // array literature
        Literature[] literatures = new Literature[3];
        literatures[0] = new Book("War And Peace", "Lev Tolstoy", "Русский вестник", 1869);
        literatures[1] = new Magazine("Title Magazine", "Theme Magazine", 2011);
        literatures[2] = new YearBook("Title Directore", "Theme Directory", "Crown Publishing Group", 1997);

        //get Info
        for (Literature literature : literatures) {
            System.out.println(literature.getInfo());
        }

        // literature entered year
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter year: ");
        int enterYear = scan.nextInt();
        scan.nextLine();

        for (Literature literature : literatures) {
            if (enterYear <= currentYear && enterYear <= literature.getYear()) {
                System.out.println(literature.getInfo());
            }
        }
    }
}