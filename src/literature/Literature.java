package literature;

/**
 * Created by Максим on 21.03.2017.
 */
public class Literature {
    private String title;
    private int year;

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    public Literature(String title, int year) {
        this.title = title;
        this.year = year;
    }

    protected String getInfo() {
        return title + ", " + year;
    }
}

