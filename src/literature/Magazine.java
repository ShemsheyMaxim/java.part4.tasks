package literature;

/**
 * Created by Максим on 21.03.2017.
 */
public class Magazine extends Literature {

    private String theme;

    public String getTheme() {
        return theme;
    }

    public Magazine(String title, String theme, int year) {
        super(title, year);
        this.theme = theme;
    }

    @Override
    protected String getInfo() {
        return "Magazine: " + super.getInfo() + ", " + theme + ".";
    }
}

