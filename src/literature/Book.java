package literature;

/**
 * Created by Максим on 21.03.2017.
 */
public class Book extends Literature {
    private String author;
    private String publisher;

    public String getAuthor() {
        return author;
    }

    public String getPublisher() {
        return publisher;
    }

    public Book(String title, String author, String publisher, int year) {
        super(title, year);
        this.author = author;
        this.publisher = publisher;
    }

    @Override
    public String getInfo() {
        return "Book: " + super.getInfo() + ", " + author + ", " + publisher + ".";
    }
}