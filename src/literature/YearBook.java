package literature;

/**
 * Created by Максим on 21.03.2017.
 */
public class YearBook extends Literature {
    private String theme;
    private String publisher;

    public String getTheme() {
        return theme;
    }

    public String getPublisher() {
        return publisher;
    }

    public YearBook(String title, String theme, String publisher, int year) {
        super(title, year);
        this.theme = theme;
        this.publisher = publisher;
    }

    @Override
    protected String getInfo() {
        return "Yearbook: " + super.getInfo() + ", " + theme + ", " + publisher + ".";
    }
}

